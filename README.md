<p align="center"><a href="https://vuejs.org" target="_blank" rel="noopener noreferrer"><img width="100" src="https://img.upanh.tv/2022/02/11/lampart-logo.jpg" alt="lampart-logo.jpg" alt="Lampart logo"></a></p>


## This repo is for demo Editor

## Run
cd backend

composer install

php artisan serve

----------------------

cd client

npm install

npm run dev

## Quill

Quill is a free, open source WYSIWYG editor built for the modern web. With its modular architecture and expressive API, it is completely customizable to fit any need.

<p align="center">
  <a target="_blank" href="https://quilljs.com/">
    <img alt="quill" src="https://img.upanh.tv/2022/02/11/quill.jpg">
  </a>
</p>

## Tiny-mce

The rich text editor behind great content creation experiences

<p align="center">
  <a target="_blank" href="https://www.tiny.cloud/tinymce/">
    <img alt="tinymce" src="https://img.upanh.tv/2022/02/11/tinymce.jpg" alt="tinymce.jpg">
  </a>
</p>

---

## Compare Editor


<img src="https://img.upanh.tv/2022/02/11/compareEditor1.jpg" alt="compare1">


<img src="https://img.upanh.tv/2022/02/11/compareEditor2.jpg" alt="compare2">


<img src="https://img.upanh.tv/2022/02/11/compareEditor3.png" alt="compare3">


<img src="https://img.upanh.tv/2022/02/11/compareEditor4.png" alt="compare4">


<img src="https://img.upanh.tv/2022/02/11/compareEditor5.jpg" alt="compare5">


<img src="https://img.upanh.tv/2022/02/11/compareEditor6.jpg" alt="compare6">


<img src="https://img.upanh.tv/2022/02/11/compareEditor7.jpg" alt="compare7">


<img src="https://img.upanh.tv/2022/02/11/compareEditor8.jpg" alt="compare8">


<img src="https://img.upanh.tv/2022/02/11/compareEditor9.jpg" alt="compare9">


<img src="https://img.upanh.tv/2022/02/11/compareEditor10.jpg" alt="compare10">


## Contribution

<img src="https://img.upanh.tv/2022/02/11/banner.png" alt="teamdon.png">

## License

Copyright (c) @2022 Lampart
