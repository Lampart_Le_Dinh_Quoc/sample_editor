<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageUploadController
{
    public function upload(Request $request) {
        // Create directory
        if(!Storage::disk('public')->exists('images')) {
            Storage::disk('public')->makeDirectory('images', 0775, true);
        }
        
        if ($request->hasFile('file') || $request->hasFile('upload')) {
            $name = Storage::disk('public')->putFile('images', $request->file('file') ?? $request->file('upload'));
            
            $link = "http://localhost:8000/storage/$name";

            return response()->json([
                'location' => $link,
                'url' => $link
            ]);
        }

        return response()->json('fail');
    }
}
