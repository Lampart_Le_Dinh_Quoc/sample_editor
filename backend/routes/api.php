<?php

use App\Http\Controllers\ImageUploadController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Test upload image
Route::post('/upload', function (Request $request) {
    $filePath = null;
    if(isset($request->file)){
        $file = $request->file;

        try{
            $fileName = md5($file->getClientOriginalName()).'.'. $file->extension();
            $file->move('upload', $fileName);
            $filePath = asset("upload/$fileName");
        }catch (\Exception $e){
            Log::error($e);
        }
    }

    $isEditorJS = $request->input('is_editorjs', false);
    if ($isEditorJS) {
        return json_encode([ 'success' => 1,
            'file'    => [ 'url' => $filePath]
        ]);
    }

    return $filePath;
});

Route::post('/upload/ckAndTiny', [ImageUploadController::class, 'upload']);
