import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _6221c266 = () => interopDefault(import('..\\pages\\ck-editor.vue' /* webpackChunkName: "pages/ck-editor" */))
const _753d49a2 = () => interopDefault(import('..\\pages\\editor.vue' /* webpackChunkName: "pages/editor" */))
const _727f3940 = () => interopDefault(import('..\\pages\\quill.vue' /* webpackChunkName: "pages/quill" */))
const _aae8e092 = () => interopDefault(import('..\\pages\\tiny-mce.vue' /* webpackChunkName: "pages/tiny-mce" */))
const _6a0947dd = () => interopDefault(import('..\\pages\\tiptap.vue' /* webpackChunkName: "pages/tiptap" */))
const _77a0bc8a = () => interopDefault(import('..\\pages\\toast-ui-editor.vue' /* webpackChunkName: "pages/toast-ui-editor" */))
const _4863d489 = () => interopDefault(import('..\\pages\\vue2-editor.vue' /* webpackChunkName: "pages/vue2-editor" */))
const _39acb6ad = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/ck-editor",
    component: _6221c266,
    name: "ck-editor"
  }, {
    path: "/editor",
    component: _753d49a2,
    name: "editor"
  }, {
    path: "/quill",
    component: _727f3940,
    name: "quill"
  }, {
    path: "/tiny-mce",
    component: _aae8e092,
    name: "tiny-mce"
  }, {
    path: "/tiptap",
    component: _6a0947dd,
    name: "tiptap"
  }, {
    path: "/toast-ui-editor",
    component: _77a0bc8a,
    name: "toast-ui-editor"
  }, {
    path: "/vue2-editor",
    component: _4863d489,
    name: "vue2-editor"
  }, {
    path: "/",
    component: _39acb6ad,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
