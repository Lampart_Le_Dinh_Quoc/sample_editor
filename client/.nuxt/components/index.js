export { default as NuxtLogo } from '../..\\components\\NuxtLogo.vue'
export { default as SampleQuill } from '../..\\components\\sample-quill.vue'
export { default as Tutorial } from '../..\\components\\Tutorial.vue'
export { default as VuetifyLogo } from '../..\\components\\VuetifyLogo.vue'
export { default as SampleCkeditor } from '../..\\components\\sample\\sample-ckeditor.vue'
export { default as SampleEditor } from '../..\\components\\sample\\sample-editor.vue'
export { default as SampleTinyMce } from '../..\\components\\sample\\sample-tiny-mce.vue'
export { default as SampleTiptap } from '../..\\components\\sample\\sample-tiptap.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
