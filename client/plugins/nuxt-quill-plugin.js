import Vue from 'vue'
import VueQuillEditor from 'vue-quill-editor'
import ImageUploader from 'quill-image-uploader'
import Quill from 'quill'

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

Quill.register("modules/imageUploader", ImageUploader);
Vue.use(VueQuillEditor, /* { default global options } */)