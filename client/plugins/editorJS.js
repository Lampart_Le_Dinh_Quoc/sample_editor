import EditorJS from '@editorjs/editorjs';
import Header from '@editorjs/header';
import Table from '@editorjs/table';
import ImageTool from '@editorjs/image';
import Warning from '@editorjs/warning';
import Quote from '@editorjs/quote';
import Marker from '@editorjs/marker'

export default ({app}, inject) => {
    const defaultOptions = {
        id: '',
        data: {},
        onChange: () => {},
    }
    const editor = (options = defaultOptions) => {
        return new EditorJS({
            placeholder: 'Let`s write an awesome story!',
            /**
             * Id of Element that should contain sample instance
             */
            holder: options.id,
            /**
             * Available Tools list.
             * Pass Tool's class or Settings object for each Tool you want to use
             */
            tools: {
                header: Header,
                table: Table,
                image: {
                    class: ImageTool,
                    config: {
                        /**
                         * Custom uploader
                         */
                        uploader: {
                            /**
                             * Upload file to the server and return an uploaded image data
                             * @param {File} file - file selected from the device or pasted by drag-n-drop
                             * @return {Promise.<{success, file: {url}}>}
                             */
                            uploadByFile(file) {
                                // your own uploading logic here
                                let param = new FormData()
                                param.append('file', file)
                                param.append('is_editorjs', true)

                                return app.$axios.$post('http://localhost:8000/api/upload', param)
                            },

                            /**
                             * Send URL-string to the server. Backend should load image by this URL and return an uploaded image data
                             * @param {string} url - pasted image URL
                             * @return {Promise.<{success, file: {url}}>}
                             */
                            uploadByUrl(url){
                                return new Promise((resolve => {
                                    resolve({
                                        success: 1,
                                            file: {
                                                url: url
                                            }
                                    })
                                }))
                            }
                        },
                    },
                },
                warning: Warning,
                quote: Quote,
                Marker: {
                    class: Marker,
                    shortcut: 'CMD+SHIFT+M',
                }
            },
            /**
             * Previously saved data that should be rendered
             */
            data: options.data || {},
            onChange(data) {
                // pass in function from component to run on change
                options.onChange(data)
            }
        })
    };

    inject('editor', options => editor(options));
}
