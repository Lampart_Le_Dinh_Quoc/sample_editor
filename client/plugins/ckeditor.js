import Vue from "vue";
import CKEditor from "@ckeditor/ckeditor5-vue2";
import * as editor from './ckeditor/ckeditor'

Vue.use(CKEditor);

export default ({app}, inject) => {
  inject('classicEditor', editor)
}
